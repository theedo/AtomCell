package Grid;

import Automata.*;
import Grid.Cell.Cell;
import Sounds.AutomataRhythmic;

import javax.swing.*;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.util.List;


/**
 * Created by Edoardo on 07/05/2018.
 */
public class GridSystem extends JPanel implements ComponentListener, MouseListener, MouseMotionListener, MouseWheelListener, Runnable{

    private Dimension boardSize = null;
    private int squareDimension;
    private ArrayList<Cell> point = new ArrayList<>(0);
    private Color ALIVE;
    private Color DEATH;
    private boolean stopThread = false;
    private int checkCount = 0;
    private int speed;
    private int aroundsType;
    private int automaType;
    private List<Integer> listAliveCell;
    private HashMap<Integer, boolean[][]> mapsDictionary = new HashMap<>();
    private int countSimulation = 0;

    public GridSystem(int squareDimension, Color alive, Color death, int arounds){
        this.squareDimension = squareDimension;
        addComponentListener(this);
        addMouseListener(this);
        addMouseMotionListener(this);
        addMouseWheelListener(this);
        this.ALIVE = alive;
        this.DEATH = death;
        this.speed = 100;
        this.aroundsType = arounds;
        boardSize = new Dimension(getWidth()/squareDimension - 2, getHeight()/squareDimension - 2);
        this.listAliveCell = new ArrayList<>();
    }

    public void setSpeed(int i){
        this.speed = i;
    }

    public Color getAlive(){
        return this.ALIVE;
    }

    public Color getDeath(){
        return this.DEATH;
    }

    public void setAutomaType(int i){
        this.automaType = i;
    }

    private void addPoint(int x, int y){
        Cell t = new Cell(x, y);
        if(!point.contains(t)) {
            point.add(t);
        }
        /*} else {
            t.setDeath();
            point.remove(t);
        }*/
        repaint();
    }

    private void removePoint(int x, int y){
        point.remove(new Cell(x, y));
    }

    private void addDrawedPoint(MouseEvent m){
        int x = m.getPoint().x/squareDimension-1;
        int y = m.getPoint().y/squareDimension-1;
        if ((x >= 0) && (x < boardSize.width) && (y >= 0) && (y < boardSize.height)) {
            addPoint(x,y);
        }
    }

    public void clearGrid(){
        point.clear();
        listAliveCell.clear();
        repaint();
    }

    public void randomizeColor(){
        point.clear();
        for (int i = 0; i < boardSize.width; i++) {
            for (int j = 0; j < boardSize.height; j++) {
                if (Math.random()*100 < 15) {
                    addPoint(i, j);
                }
            }
        }
    }

    private int prevN = 0;



    private void updateArrayPoint(){
        // aggiorna la lista di punti per il resize.
        ArrayList<Point> t = new ArrayList<>(0);
        for(Cell tmp : point){
            if(tmp.x > boardSize.width - 1 || tmp.y > boardSize.height - 1){
                t.add(tmp);
            }
        }
        point.removeAll(t);
        repaint();
    }

    @Override
    public void paintComponent(Graphics g){
        super.paintComponent(g);
        setBackground(this.DEATH);
        try {
            for (Cell newPoint : point) {
                g.setColor(this.ALIVE); // il colore da settare
                g.fillRect(squareDimension + (squareDimension*newPoint.x), squareDimension + (squareDimension*newPoint.y), squareDimension, squareDimension);
            }
        } catch (ConcurrentModificationException ex) {

        }
        g.setColor(Color.BLACK); // colore dei bordi della griglia.
        for (int i=0; i <= boardSize.width; i++) {
            g.drawLine(((i*squareDimension)+squareDimension), squareDimension, (i*squareDimension)+squareDimension, squareDimension + (squareDimension*boardSize.height));
        }
        for (int i=0; i <= boardSize.height; i++) {
            g.drawLine(squareDimension, ((i*squareDimension)+squareDimension), squareDimension*(boardSize.width + 1), ((i*squareDimension)+squareDimension));
        }
    }

    @Override
    public void componentResized(ComponentEvent e) {
        boardSize = new Dimension(getWidth()/squareDimension - 2, getHeight()/squareDimension - 2);
        updateArrayPoint();
    }

    @Override
    public void componentMoved(ComponentEvent e) {

    }

    @Override
    public void componentShown(ComponentEvent e) {

    }

    @Override
    public void componentHidden(ComponentEvent e) {

    }

    @Override
    public void mouseClicked(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {
        addDrawedPoint(e);
    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    @Override
    public void mouseDragged(MouseEvent e) {
        addDrawedPoint(e);
    }

    @Override
    public void mouseMoved(MouseEvent e) {

    }

    public int aliveCell(){
        return point.size();
    }

    public boolean getStopThread(){
        return this.stopThread;
    }

    public void setStopThread(boolean c){
        this.stopThread = c;
    }

    @Override
    public void run() {
        ArrayList<Cell> survivingCells = new ArrayList<>(0);
        if(this.automaType == 1) {
                survivingCells = new Conway(
                    boardSize.width + 2, boardSize.height + 2, this.aroundsType).initialize(this.point);
        } else if (this.automaType == 3){
            survivingCells = new LiveFreeOrDIe(
                    boardSize.width + 2,boardSize.height + 2, this.aroundsType).initialize(this.point);
        } else if (this.automaType == 2){
            survivingCells = new Pulsar(
                    boardSize.width + 2,boardSize.height + 2, this.aroundsType).initialize(this.point);
        } else if (this.automaType == 4){
            survivingCells = new Rule30(
                    boardSize.width+2, boardSize.height+2).initialize(this.point);
        } else if (this.automaType == 5){
            survivingCells = new Rule90(
                    boardSize.width+2, boardSize.height+2).initialize(this.point);
        } else if(this.automaType == 6){
            survivingCells = new Maze(boardSize.width + 2,boardSize.height + 2, this.aroundsType).initialize(this.point);
        } else if(this.automaType == 7){
            survivingCells = new DayAndNight(boardSize.width + 2,boardSize.height + 2, this.aroundsType).initialize(this.point);
        } else if (this.automaType == 8){
            survivingCells = new Rule110(
                    boardSize.width+2, boardSize.height+2).initialize(this.point);
        } else if (this.automaType == 9){
            survivingCells = new Rule184(
                    boardSize.width+2, boardSize.height+2).initialize(this.point);
        } else if (this.automaType == 10) {
            survivingCells = new Rule150(
                    boardSize.width + 2, boardSize.height + 2).initialize(this.point);
        }
        if(survivingCells.size() == this.point.size()){
            checkCount++;
        } else {
            checkCount = 0;
        }
        if(survivingCells.size() == 0 || checkCount >= 10){
            this.stopThread = true;
        }
        point.clear();
        this.repaint();
        point.addAll(survivingCells);
        //listAliveCell.add(survivingCells.size());
        boolean[][] map = this.map(survivingCells);
        mapsDictionary.put(countSimulation, map);
        listAliveCell.add(survivingCells.size());
        countSimulation++;
        try{
            Thread.sleep(speed);
            run();
        } catch(InterruptedException ex){

        }
    }

    public void doMidi(){
        AutomataRhythmic k = new AutomataRhythmic(this.mapsDictionary, 100);
        k.doAlgorithm();
        k.doMidi();
    }


    public boolean[][] map(ArrayList<Cell> p){
        boolean[][] gameBoard = new boolean[boardSize.width + 2][boardSize.height + 2];
        for (Cell current : p) {
            gameBoard[current.x+1][current.y+1] = true;
        }
        return gameBoard;
    }

    public HashMap<Integer, boolean[][]> getMapsDictionary(){
        return mapsDictionary;
    }

    public List<Integer> getListAliveCell(){
        return this.listAliveCell;
    }

    @Override
    public void mouseWheelMoved(MouseWheelEvent e) {

    }

    public int getSpeed(){
        return this.speed;
    }
}
