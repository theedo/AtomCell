package Grid.Preferences;

import java.awt.*;

public class GridSettings {

    /*

        Classe GridSettings: tutte le informazioni per la creazione della griglia passano da qui.

     */

    private int IS_BORDERED_GRID = 1; // la griglia ha i bordi: 0 no; 1 sì.
    private Color borderColor; // il colore dei bordi;
    private Color aliveColor; // colore delle celle vive;
    private Color deathColor; // colore delle celle morte;
    private Color pointerColor;// colore del mousePointer

    public GridSettings(){
        this.borderColor = Color.BLACK;
        this.aliveColor = Color.GREEN;
        this.deathColor = Color.WHITE;
        this.pointerColor = Color.GRAY;
    }

    public GridSettings(Color aliveColor, Color deathColor, int has_border){
        this.borderColor = Color.black;
        this.pointerColor = Color.GRAY;
        this.aliveColor = aliveColor;
        this.deathColor = deathColor;
        this.IS_BORDERED_GRID = has_border;
    }

    public int isBorderedGrid(){
        return this.IS_BORDERED_GRID;
    }

    public Color getBorderColor(){
        return  this.borderColor;
    }

    public Color getAliveColor(){
        return this.aliveColor;
    }

    public Color getDeathColor(){

        return this.deathColor;
    }

    public Color getPointerColor(){
        return this.pointerColor;
    }

}
