package Grid.Cell;

import Grid.Preferences.GridSettings;

import javax.swing.*;
import java.awt.*;


public class Cell extends Point {

    private boolean isAlive;
    public Cell(int x, int y){
        this.x = x;
        this.y = y;
        isAlive = false;
    }

    public void setAlive(){
        this.isAlive = true;
    }

    public void setDeath(){
        this.isAlive = false;
    }

    public boolean isAlive(){
        return this.isAlive;
    }

}
