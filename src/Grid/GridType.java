package Grid;

/**
 * Created by Edoardo on 07/05/2018.
 */
public enum GridType {
    RECTANGLE,
    HEXAGON,
}
