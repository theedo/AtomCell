package Charts;

import org.knowm.xchart.QuickChart;
import org.knowm.xchart.SwingWrapper;
import org.knowm.xchart.XYChart;

public class ChartManager {

    private double[] xData;
    private double[] yData;

    public void setXData(double[] x){
        this.xData = x;
    }

    public void setYData(double[] y){
        this.yData = y;
    }

    public XYChart getChart(){
        XYChart t = QuickChart.getChart("Sample Chart", "X", "Y", "y(x)", xData, yData);
        new SwingWrapper<>(t).displayChart();
        return t;
    }


}
