package Sounds;

import Sounds.Inst.TriangleInst;
import jm.JMC;
import jm.constants.Pitches;
import jm.gui.show.ShowScore;
import jm.music.data.Note;
import jm.music.data.Part;
import jm.music.data.Phrase;
import jm.music.data.Score;
import jm.util.Write;

import java.time.Instant;
import java.util.*;

/**
 * Created by Edoardo on 02/06/2018.
 */
public class AutomataRhythmic implements JMC{

    private boolean[][] map;
    private int maxNotes;
    private Phrase one;
    private Part piano;
    private Phrase two;
    private Score score;
    private TriangleInst triangle;
    private double[][] markovMatrix;
    private int[] possiblePitch;
    private HashMap<Integer, boolean[][]> mapsDictionary;

    public AutomataRhythmic(HashMap<Integer, boolean[][]> maps, int maxNotes){
        this.mapsDictionary = maps;
        this.maxNotes = maxNotes;
        this.score = new Score("Automata cellular", 120);
        this.piano = new Part("piano", GUITAR, PIANO);
        one = new Phrase();
        this.triangle = new TriangleInst(44100);
    }

    public double[][] doMarkovMatrix(boolean[][] map){
        this.markovMatrix = new double[map.length][map[0].length];
        for(int k = 0; k < map.length - 1; k++){
            for(int j = 0; j < map[0].length - 1; j++){
                markovMatrix[k][j] = Math.random();
            }
        }
        return this.markovMatrix;
    }

    public double[][] markovMatrix(boolean[][] map){
        double[][] to_return = new double[map.length][map[0].length];
        for(int x = 0; x < map.length - 1; x++){
            for(int y = 0; y < map[0].length - 1; y++){
                if(map[x][y]){
                    to_return[x][y] = Math.random();
                } else {
                    to_return[x][y] = 0;
                }
            }
        }
        return to_return;
    }

    public void doAlgorithm(){
        for(int i = 0; i < maxNotes; i++){
            List<Integer> idxNote = new ArrayList<>();
            List<Integer> idxDeathCell = new ArrayList<>();
            List<Integer> valueGeneral = new ArrayList<>();
            for(Map.Entry<Integer, boolean[][]> map: mapsDictionary.entrySet()){
                boolean[][] prev = null;
                boolean[][] next = null;
                double[][] nextMarkoved = null;
                double[][] prevMarkoved = null;
                if(map.getKey() > 0){
                    prev = mapsDictionary.get(map.getKey() - 1);
                }
                if(mapsDictionary.containsKey(map.getKey()+1)){
                    next = mapsDictionary.get(map.getKey() + 1);
                }
                double[][] mapMarkoved = markovMatrix(map.getValue());
                if(prev != null){
                    prevMarkoved = markovMatrix(prev);
                }
                if(next != null){
                    nextMarkoved = markovMatrix(next);
                }
                int liveCell = 0;
                for(int x = 0; x < map.getValue().length - 1; x++) {
                    double countRow = 0;
                    int deathCell = 0;
                    for(int y = 0; y < map.getValue()[0].length - 1; y++){
                        if(map.getValue()[x][y]){
                            liveCell += 1;
                        } else {
                            deathCell++;
                        }
                        if(nextMarkoved != null && prevMarkoved != null) {
                            countRow += mapMarkoved[x][y] + nextMarkoved[x][y] + prevMarkoved[x][y];
                        } else if(nextMarkoved != null && prevMarkoved == null){
                            countRow += mapMarkoved[x][y] + nextMarkoved[x][y];
                        } else if(nextMarkoved == null && prevMarkoved != null){
                            countRow += mapMarkoved[x][y] + prevMarkoved[x][y];
                        } else {
                            countRow += mapMarkoved[x][y];
                        }
                    }
                    double print = 127 - countRow;
                    int k = (int)print;
                    int d;
                    if(k > liveCell){
                        d = k - liveCell;
                    } else {
                        d = liveCell - k;
                    }
                    idxNote.add(d);
                    idxDeathCell.add(deathCell);
                }
                Double average = idxNote.stream().mapToInt(val -> val).average().orElse(0.0);
                valueGeneral.add(average.intValue());
            }
            Double average = valueGeneral.stream().mapToInt(val -> val).average().orElse(0.0);
            int f = average.intValue() - randInt(0, average.intValue());
            //int[] pitchArray = {E4,FS4,B4,CS5,D5,FS4,E4,CS5,B4,FS4,D5,CS5};
            Note c = new Note(f, SQ);
            //one.addNoteList(pitchArray, rhythmArray);
           // c.setPitch(average.intValue());
            one.add(c);
        }
    }


    public Phrase getOne(){
        return this.one;
    }

    public void doMidi(){
        piano.addPhrase(one);
        score.add(piano);
        String t = getSaltString();
        Write.midi(score, "AutomataMusic_" + t + ".midi");
        Write.au(score, "AutomataMusic_"+ t +".au", triangle);
    }

    public static int randInt(int min, int max) {

        // NOTE: This will (intentionally) not run as written so that folks
        // copy-pasting have to think about how to initialize their
        // Random instance.  Initialization of the Random instance is outside
        // the main scope of the question, but some decent options are to have
        // a field that is initialized once and then re-used as needed or to
        // use ThreadLocalRandom (if using at least Java 1.7).
        //
        // In particular, do NOT do 'Random rand = new Random()' here or you
        // will get not very good / not very random results.
        Random rand = new Random();

        // nextInt is normally exclusive of the top value,
        // so add 1 to make it inclusive
        int randomNum = rand.nextInt((max - min) + 1) + min;

        return randomNum;
    }

    protected String getSaltString() {
        String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 3) { // length of the random string.
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();
        return saltStr;

    }

}
