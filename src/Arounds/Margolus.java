package Arounds;

import Grid.Cell.CellType;
import Grid.Cell.Cell;

import java.util.ArrayList;
import java.util.HashMap;

public class Margolus {


    public Margolus(){

    }

    public void doMargolus(boolean[][] gameBoard, int i, int j, ArrayList<Cell> survivingCells){
        int surrounding = 0;
        if(gameBoard[i+1][j-1]){
            surrounding++;
        }
        if(gameBoard[i][j-1]){
            surrounding++;
        }
        if(gameBoard[i+1][j]) {
            surrounding++;
        }

        if (gameBoard[i][j]) {
            if ((surrounding == 2) || (surrounding == 3)) {
                survivingCells.add(new Cell(i-1,j-1));
            }
        } else {
            if (surrounding == 3) {
                survivingCells.add(new Cell(i-1,j-1));
            }
        }
    }
}
