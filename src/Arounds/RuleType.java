package Arounds;

import Grid.Cell.Cell;

import java.util.ArrayList;

public interface RuleType {

    void rule(boolean[][] gameBoard, int i, int j, ArrayList<Cell> survivingCells);

}
