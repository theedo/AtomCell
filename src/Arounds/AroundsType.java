package Arounds;

import Grid.Cell.Cell;

import java.util.ArrayList;

public interface AroundsType {

    void doMoore(boolean[][] gameBoard, int i, int j, ArrayList<Cell> survivingCells);

    void doMargolus(boolean[][] gameBoard, int i, int j, ArrayList<Cell> survivingCells);

    void doVonNeumann(boolean[][] gameBoard, int i, int j, ArrayList<Cell> survivingCells);

    void doHexagonal(boolean[][] gameBoard, int i, int j, ArrayList<Cell> survivingCells);

}


