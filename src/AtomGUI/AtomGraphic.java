package AtomGUI;

import Grid.GridSystem;

import Sounds.AutomataRhythmic;
import org.knowm.xchart.QuickChart;
import org.knowm.xchart.SwingWrapper;
import org.knowm.xchart.XYChart;

import javax.sound.sampled.*;
import javax.swing.*;
import javax.swing.event.ChangeEvent;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;


/**
 * Created by Edoardo on 07/05/2018.
 */
public class AtomGraphic extends JFrame{
    private GridSystem gridList;
    private JSlider speedSlider;
    private JSlider volumeSlider;
    private JButton clearGrid;
    private JButton randomizeColor;
    private JButton startSimulation;
    private JLabel allCellCount;
    private boolean running = false;
    private Color alive;
    private Color dead;
    private int has_border;
    private Thread runGame;
    private ActionListener task;
    private ActionListener taskStopThread;
    private int automaType;
    private Clip clip;
    private JButton chartsButton;
    private ArrayList<String> songs = new ArrayList<>(0);
    private int index = 0;
    private JButton generateMusicButton;


    public AtomGraphic(int squareDimension, Color alive, Color dead, int around, int automaType){
        this.alive = alive;
        this.dead = dead;
        this.has_border = has_border;
        this.automaType = automaType;
        try {
            clip = AudioSystem.getClip();
        } catch(LineUnavailableException e){}
        setCellGridPanelController(new GridSystem(squareDimension, alive, dead, around));
        this.songs.add("Bangarang.wav");
        this.songs.add("Song_Two.wav");
        this.songs.add("Song_Three.wav");
        this.songs.add("MrRobot.wav");
        this.songs.add("Matrix.wav");
        initGraphic();
    }

    private void initGraphic(){
        int MIN_SIZE = 400;
        this.setMinimumSize(new Dimension(MIN_SIZE, MIN_SIZE));
        this.setSize(new Dimension(getMaximumSize().width, getMaximumSize().height));
        this.setTitle("AtomCell");
        JPanel jPanel = new JPanel();
        jPanel.setLayout(new BorderLayout());

        JPanel southBar = new JPanel(new FlowLayout(FlowLayout.RIGHT)); // aggiunge la barra sotto alla griglia
        JPanel northBar = new JPanel(new FlowLayout(FlowLayout.RIGHT)); // aggiunge la barra sopra


        setAllCellCount(new JLabel("Alive cells: " + gridList.aliveCell()));
        getAllCellCount().setForeground(gridList.getAlive());
        southBar.add(getAllCellCount());

        northBar.add(new JLabel("Music's gain"));
        setVolumeSlider(new JSlider(0, 160, 50));
        this.volumeSlider.addChangeListener((ChangeEvent change) -> {
            if (clip.isRunning()){
                FloatControl volume = (FloatControl)clip.getControl(FloatControl.Type.MASTER_GAIN);
                volume.setValue((float)(this.volumeSlider.getValue()/10)-10.0f);
            }
        });
        northBar.add(getVolumeSlider());

        setGenerateMusicButton(new JButton("Generate song"));
        if(gridList.getMapsDictionary().size() == 0 && !running){
            generateMusicButton().setEnabled(false);
        }

        generateMusicButton().addActionListener(l -> {
            if(gridList.getMapsDictionary().size() > 0){
                gridList.doMidi();

            } else {
                new ExceptionDialog("No maps loaded.", "No maps.");
            }
        });

        JLabel increaseVol = new JLabel();
        increaseVol.setIcon(new ImageIcon(new ImageIcon(getClass().getResource(
                "Immagini/increaseVol.png")
        ).getImage().getScaledInstance(40, 40, Image.SCALE_AREA_AVERAGING)));
        increaseVol.setCursor(new Cursor(Cursor.HAND_CURSOR));
        increaseVol.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
               volumeSlider.setValue(
                       (volumeSlider.getValue()<10) ? volumeSlider.getValue()+1 : volumeSlider.getValue()+10);
               if (clip.isRunning()){
                   FloatControl volume = (FloatControl)clip.getControl(FloatControl.Type.MASTER_GAIN);
                   volume.setValue((float) (volumeSlider.getValue() / 10) - 10.0f);
               }
            }
            @Override
            public void mousePressed(MouseEvent e) {}
            @Override
            public void mouseReleased(MouseEvent e) {}
            @Override
            public void mouseEntered(MouseEvent e) {}
            @Override
            public void mouseExited(MouseEvent e) {}
        });
        northBar.add(increaseVol);

        JLabel decreaseVol = new JLabel();
        decreaseVol.setIcon(new ImageIcon(new ImageIcon(getClass().getResource(
                "Immagini/decreaseVol.png")
        ).getImage().getScaledInstance(30, 30, Image.SCALE_AREA_AVERAGING)));
        decreaseVol.setCursor(new Cursor(Cursor.HAND_CURSOR));
        decreaseVol.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                volumeSlider.setValue(
                        (volumeSlider.getValue() >= 20) ? volumeSlider.getValue()-10 : volumeSlider.getValue()-1);
                if (clip.isRunning()){
                    FloatControl volume = (FloatControl)clip.getControl(FloatControl.Type.MASTER_GAIN);
                    if (volumeSlider.getValue() == 0){
                        volume.setValue(-50.0f);
                    } else {
                        volume.setValue((float) (volumeSlider.getValue() / 10) - 10.0f);
                    }
                }
            }
            @Override
            public void mousePressed(MouseEvent e) {}
            @Override
            public void mouseReleased(MouseEvent e) {}
            @Override
            public void mouseEntered(MouseEvent e) {}
            @Override
            public void mouseExited(MouseEvent e) {}
        });
        northBar.add(decreaseVol);

        setChartsButton(new JButton("Create chart"));
        getChartsButton().setEnabled(false);
        northBar.add(getChartsButton());
        northBar.add(generateMusicButton());
        getChartsButton().addActionListener((ActionEvent e) -> {
            double[] xData = new double[gridList.getListAliveCell().size()];
            double[] yData = new double[gridList.getListAliveCell().size()];
            int countX = 0;
            for(Integer x: gridList.getListAliveCell()){
                xData[countX] = countX;
                yData[countX] = x.doubleValue();
                countX++;
            }
            Thread xk = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        XYChart t = QuickChart.getChart("Chart Alive Cell", "Time", "Number Alive", "t(n)", xData, yData);
                        new SwingWrapper<>(t).displayChart().setDefaultCloseOperation(2);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
            xk.start();
        });

        //speeder velocità automa
        southBar.add(new JLabel("Simulation speed:"));
        setSpeedSlider(new JSlider(60, 500, 125));
        getSpeedSlider().setMinorTickSpacing(75);
        gridList.setSpeed(125);
        getSpeedSlider().addChangeListener(c -> {
            gridList.setSpeed(getSpeedSlider().getValue());
            System.out.println(getSpeedSlider().getValue());
        });

        getSpeedSlider().setSnapToTicks(true);
        southBar.add(getSpeedSlider());
        /* termine speeder velocità automa */

        /* bottone per rimuovere tutte le celle */
        setClearGrid(new JButton("Empty grid"));
        getClearGrid().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                gridList.clearGrid(); // quando premo il bottone svuota la griglia.
            }
        });
        southBar.add(getClearGrid());
        /* fine bottone */


        /* bottone randomizzazione */
        setRandomizeColor(new JButton("Randomize cells"));
        getRandomizeColor().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                gridList.randomizeColor();
                getAllCellCount().setText("Alive cells: " + gridList.aliveCell());
            }
        });
        southBar.add(getRandomizeColor());
        /* fine bottone randomizzazione */

        /* bottone per iniziare la simulazione */
        setStartSimulation(new JButton("Start simulation"));
        getStartSimulation().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                startSimulationEvent();
            }
        });
        southBar.add(getStartSimulation());
        /* fine bottone per iniziare simulazione */

        gridList.setCursor(new Cursor(Cursor.HAND_CURSOR));
        gridList.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                getAllCellCount().setText("Alive cells: " + gridList.aliveCell());
            }

            @Override
            public void mousePressed(MouseEvent e) {
                getAllCellCount().setText("Alive cells: " + gridList.aliveCell());
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                getAllCellCount().setText("Alive cells: " + gridList.aliveCell());
            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });

        jPanel.add(gridList, BorderLayout.CENTER); // aggiunge la griglia al centro
        jPanel.add(southBar, BorderLayout.SOUTH); // aggiunge la southBar sotto
        jPanel.add(northBar, BorderLayout.NORTH);

        this.setContentPane(jPanel); // carica il jPanel nel Frame
        this.setLocationRelativeTo(null);
        this.setVisible(true);
        this.pack();
        task = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                getAllCellCount().setText("Alive cells: " + gridList.aliveCell());
            }
        };

        taskStopThread = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(gridList.getStopThread()){
                    //runGame.interrupt();
                    gridList.setStopThread(false);
                    getStartSimulation().doClick(1);
                }
            }
        };

        gridList.setAutomaType(this.automaType);
    }

    private void setCellGridPanelController(GridSystem cellGridPanelController) {
        this.gridList = cellGridPanelController;
    }

    private void setSpeedSlider(JSlider speed){
        this.speedSlider = speed;
    }

    public JSlider getSpeedSlider(){
        return this.speedSlider;
    }

    public void setClearGrid(JButton gridClearButton){
        this.clearGrid = gridClearButton;
    }

    public JButton getClearGrid(){
        return this.clearGrid;
    }

    public void setStartSimulation(JButton startSimulation){ this.startSimulation = startSimulation; }

    public JButton getStartSimulation(){
        return this.startSimulation;
    }

    public void setRandomizeColor(JButton buttonT){
        this.randomizeColor = buttonT;
    }

    public JButton getRandomizeColor(){
        return this.randomizeColor;
    }

    public JLabel getAllCellCount() {
        return this.allCellCount;
    }

    public void setAllCellCount(JLabel t){
        this.allCellCount = t;
    }

    public void setVolumeSlider(JSlider volume){ this.volumeSlider = volume; }

    public JSlider getVolumeSlider(){ return this.volumeSlider; }

    public void setChartsButton(JButton t){
        this.chartsButton = t;
    }

    public JButton getChartsButton(){
        return this.chartsButton;
    }

    public JButton generateMusicButton(){
        return this.generateMusicButton;
    }

    public void setGenerateMusicButton(JButton t){
        this.generateMusicButton = t;
    }

    private void startSimulationEvent(){
        Timer t = new Timer(1, task);
        Timer t1 = new Timer(1, taskStopThread);
        try {
            AudioInputStream audio = AudioSystem.getAudioInputStream(new File(
                    getClass().getResource("../Sounds/MrRobot.wav").getFile()));
            if(!clip.isOpen()) {
                clip.open(audio);
                FloatControl volume = (FloatControl)clip.getControl(FloatControl.Type.MASTER_GAIN);
                volume.setValue((float) (volumeSlider.getValue() / 10) - 10.0f);
            }
            if (!running) {
                running = true;
                getSpeedSlider().setEnabled(false); // disabilita velocità
                getClearGrid().setEnabled(false); // disabilita bottone
                getRandomizeColor().setEnabled(false);
                getStartSimulation().setText("Stop simulation");
                getChartsButton().setEnabled(false);
                this.runGame = new Thread(gridList);
                runGame.start();
                t.start();
                t1.start();
                clip.start();
                generateMusicButton().setEnabled(false);
            } else {
                clip.stop();
                //gridList.doMarkovMatrix();
                running = false;
                getSpeedSlider().setEnabled(true); // abilita velocità
                getClearGrid().setEnabled(true); // abilita bottone
                getRandomizeColor().setEnabled(true);
                getStartSimulation().setText("Start simulation");
                getChartsButton().setEnabled(true);
                runGame.interrupt();
                if (t.isRunning())
                    t.stop();
                if (t1.isRunning())
                    t1.stop();
                if(gridList.getMapsDictionary().size() > 0 && !running){
                    generateMusicButton().setEnabled(true);
                }
            }
        }
        catch(UnsupportedAudioFileException uae) {
            System.out.println(uae);
        }
        catch(IOException ioe) {
            System.out.println(ioe);
        }
        catch(LineUnavailableException lua) {
            System.out.println(lua);
        }

    }
}
