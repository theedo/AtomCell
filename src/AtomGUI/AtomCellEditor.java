package AtomGUI;

import AtomGUI.Exceptions.*;
import Grid.Preferences.*;

import javax.swing.*;
import javax.swing.event.ListDataEvent;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.ImageObserver;
import java.awt.image.ImageProducer;
import java.util.ArrayList;

public class AtomCellEditor {

	private JFrame frmAtomCellEditor;
	private JButton btnCreate = new JButton("CREATE");
	JComboBox cbAround = new JComboBox();
	JComboBox cbAutomata = new JComboBox();
	JSlider slider = new JSlider(2, 10, 2);

	private Color aliveColor = null;
	private Color deadColor = null;
	private ArrayList<Integer> rules = new ArrayList<>();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AtomCellEditor window = new AtomCellEditor();
					window.frmAtomCellEditor.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public AtomCellEditor() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmAtomCellEditor = new JFrame();
		frmAtomCellEditor.setIconImage(Toolkit.getDefaultToolkit().getImage(
				getClass().getResource("Immagini/rinoceronteEmoticon.png")));
		frmAtomCellEditor.setResizable(false);
		frmAtomCellEditor.setFont(new Font("Courier New", Font.BOLD, 13));
		frmAtomCellEditor.setTitle("Atom Cell Editor");
		frmAtomCellEditor.setBounds(100, 100, 465, 345);
		frmAtomCellEditor.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmAtomCellEditor.getContentPane().setLayout(null);

		this.rules.add(4);  //Rule30
		this.rules.add(5);  //Rule90
		this.rules.add(8);	//Rule110
        this.rules.add(9);  //Rule184
        this.rules.add(10); //Rule150

		JLabel lblCellType = new JLabel("Automata Type\r\n");
		lblCellType.setFont(new Font("Courier New", Font.BOLD, 13));
		lblCellType.setBounds(23, 89, 127, 14);
		frmAtomCellEditor.getContentPane().add(lblCellType);

		cbAutomata.setFont(new Font("Times New Roman", Font.ITALIC, 13));
		cbAutomata.setModel(new DefaultComboBoxModel(new String[] {"Select an Automata ",
                "Conway's Game Of Life", "PulsarLife", "Live Free or Die", "Rule30", "HighLife", "Maze", "Day and Night",
				"Rule110", "Rule184", "Rule150"}));
		this.cbAutomata.addActionListener((ActionEvent change) -> {
		    if (this.cbAutomata.getSelectedIndex() == 1 || this.cbAutomata.getSelectedIndex() == 2 || this.cbAutomata.getSelectedIndex() == 6 || this.cbAutomata.getSelectedIndex() == 7){
		        this.cbAround.setModel(new DefaultComboBoxModel(
		                new String[] {"Select an Around's Type", "Moore's Around"}
                ));
            } else if (this.cbAutomata.getSelectedIndex() == 0 || this.cbAutomata.getSelectedIndex() == 3){
                cbAround.setModel(new DefaultComboBoxModel(
                        new String[] {"Select an Around Type",
                                "Von Neumann's Around", "Moore's Around", "Margolus' Around", "Hexagonal's Around"}));
            } else if (this.rules.contains(this.cbAutomata.getSelectedIndex())){
		        cbAround.setModel(new DefaultComboBoxModel(
		                new String[] {"Rule has no Around"}
                ));
            }
        });
		cbAutomata.setBounds(208, 86, 204, 20);
		frmAtomCellEditor.getContentPane().add(cbAutomata);

		JLabel lblTipoColore = new JLabel("Color");
		lblTipoColore.setFont(new Font("Courier New", Font.BOLD, 13));
		lblTipoColore.setBounds(26, 178, 94, 14);
		frmAtomCellEditor.getContentPane().add(lblTipoColore);


		JButton btnAlive = new JButton("Alive");
		btnAlive.setFont(new Font("Times New Roman", Font.BOLD | Font.ITALIC, 16));
		btnAlive.setBounds(208, 173, 94, 20);
		frmAtomCellEditor.getContentPane().add(btnAlive);
		btnAlive.addActionListener((ActionEvent e) -> {
			Color color = JColorChooser.showDialog(null, "Choose a color", Color.WHITE);
			this.aliveColor = color;
			if (this.aliveColor != null) {
				btnAlive.setForeground(color);
				btnAlive.setFont(new Font("Times New Roman", Font.ITALIC | Font.BOLD, 18));
				btnAlive.setSize(btnAlive.getMaximumSize().width, btnAlive.getMaximumSize().height);
			} else {
			    btnAlive.setForeground(Color.BLACK);
			    btnAlive.setFont(new Font("Times New Roman", Font.ITALIC | Font.BOLD, 16));
			    btnAlive.setBounds(208, 173, 94, 20);
            }
		});

		JLabel lblGridDimension = new JLabel("Square Size\r\n");
		lblGridDimension.setFont(new Font("Courier New", Font.BOLD, 13));
		lblGridDimension.setBounds(23, 44, 159, 18);
		frmAtomCellEditor.getContentPane().add(lblGridDimension);

		btnCreate.setForeground(Color.RED);
		btnCreate.setFont(new Font("Courier New", Font.BOLD, 40));
		btnCreate.setBounds(63, 221, 328, 68);
		btnCreate.setContentAreaFilled(true);
		btnCreate.addActionListener((ActionEvent event) -> {
			try {
				this.createAtomGraphics();
			} catch (AutomataTypeNotSelectedException atn) {
				new ExceptionDialog("Automata type not selected, please select a valid type", "Automata Type Not Selected");
			} catch (AroundTypeNotSelectedException atns) {
				new ExceptionDialog("Around type not selected, please select a valid type", "Around Type Not Selected");
			} catch (NoAliveColorInsertedException naci) {
				new ExceptionDialog("Alive color not inserted, please select a color", "Alive Color Exception");
			} catch (NoDeadColorInsertedException ndci) {
				new ExceptionDialog("Dead color not inserted, please select a color", "Dead Color Exception");
			}
		});
		frmAtomCellEditor.getContentPane().add(btnCreate);

		JButton btnDead = new JButton("Dead");
		btnDead.setFont(new Font("Times New Roman", Font.BOLD | Font.ITALIC, 16));
		btnDead.addActionListener((ActionEvent event) -> {
			Color color = JColorChooser.showDialog(null, "Choose a color", Color.WHITE);
			this.deadColor = color;
			if (this.deadColor != null) {
				btnDead.setForeground(color);
				btnDead.setFont(new Font("Times New Roman", Font.ITALIC | Font.BOLD, 18));
				btnDead.setSize(btnDead.getMaximumSize().width, btnDead.getMaximumSize().height);
			} else {
			    btnDead.setForeground(Color.BLACK);
			    btnDead.setFont(new Font("Times New Roman", Font.ITALIC | Font.BOLD, 16));
			    btnDead.setBounds(317, 173, 94, 20);
            }
		});
		btnDead.setBounds(317, 173, 94, 20);
		frmAtomCellEditor.getContentPane().add(btnDead);
		
		JLabel lblAroundType = new JLabel("Around Type");
		lblAroundType.setFont(new Font("Courier New", Font.BOLD, 13));
		lblAroundType.setBounds(23, 127, 105, 20);
		frmAtomCellEditor.getContentPane().add(lblAroundType);
		
		cbAround.setFont(new Font("Times New Roman", Font.ITALIC, 13));
        cbAround.setModel(new DefaultComboBoxModel(
                new String[] {"Select an Around Type", "Von Neumann's Around", "Moore's Around", "Margolus' Around",
                              "Hexagonal's Around"}));
		cbAround.setBounds(208, 127, 204, 20);
		frmAtomCellEditor.getContentPane().add(cbAround);
		
		slider.setBounds(208, 44, 200, 26);
		slider.getValue();
		frmAtomCellEditor.getContentPane().add(slider);
	}

	public void createAtomGraphics() throws NoAliveColorInsertedException, NoDeadColorInsertedException,
											AroundTypeNotSelectedException, AutomataTypeNotSelectedException{
		if (this.cbAutomata.getSelectedIndex() == 0) {
			throw new AutomataTypeNotSelectedException();
		} else if (this.cbAround.getSelectedIndex() == 0 && !this.rules.contains(this.cbAutomata.getSelectedIndex())){
			throw new AroundTypeNotSelectedException();
		} else if (this.aliveColor == null){		
			throw new NoAliveColorInsertedException();
		} else if (this.deadColor == null){
			throw new NoDeadColorInsertedException();
		} else {
            if (this.cbAround.getItemCount() == 2) {
                AtomGraphic gridGraphics = new AtomGraphic(
                        this.slider.getValue(), this.aliveColor, this.deadColor,
                        this.cbAround.getSelectedIndex()+1, this.cbAutomata.getSelectedIndex());
            } else {
                new AtomGraphic(this.slider.getValue(), this.aliveColor, this.deadColor,
                        this.cbAround.getSelectedIndex(), this.cbAutomata.getSelectedIndex());
            }
		}
	}
}
