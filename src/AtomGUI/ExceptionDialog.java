package AtomGUI;

import javax.swing.*;
import java.awt.*;

public class ExceptionDialog extends JOptionPane {
	public ExceptionDialog(String message, String title) {
		super();
		this.showMessageDialog(null, message, title, JOptionPane.ERROR_MESSAGE);
	}
}
