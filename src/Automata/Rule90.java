package Automata;

import Arounds.RuleType;
import Grid.Cell.Cell;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Rule90 implements RuleType {

    private int h;
    private int w;

    public Rule90(int weight, int hight){
        this.h = hight;
        this.w = weight;
    }

    public ArrayList<Cell> initialize(ArrayList<Cell> aliveCell){
        boolean[][] gameBoard = new boolean[w][h];
        ArrayList<Cell> survivingCells = new ArrayList<>(0);
        for (Cell c: aliveCell){
            gameBoard[c.x+1][c.y+1] = true;
        }
        for (int i=1; i<w-1; i++){
            for (int j=1; j<h-1; j++){
                this.rule(gameBoard, i, j, survivingCells);
            }
        }
        return survivingCells;
    }

    @Override
    public void rule(boolean[][] gameBoard, int i, int j, ArrayList<Cell> survivingCells) {
        int surrounding = 0;
        String rule = "";
        List live = Arrays.asList("110", "100", "011", "001");
        if(gameBoard[i-1][j-1]){
            rule += "1";
        } else {
            rule += "0";
        }
        if(gameBoard[i][j-1]){
            rule += "1";
        } else {
            rule += "0";
        }

        if(gameBoard[i+1][j+1]){
            rule += "1";
        } else {
            rule += "0";
        }
        if(live.contains(rule)){
            survivingCells.add(new Cell(i-1, j-1));
        }
    }
}
