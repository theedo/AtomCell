package Automata;

import Arounds.AroundsType;
import Arounds.Moore;
import Grid.Cell.Cell;

import java.util.ArrayList;

public class Conway implements AroundsType {

    private int aroundsType;
    private int w;
    private int h;

    public Conway(int w, int h, int aroundsType){
        this.w = w;
        this.h = h;
        this.aroundsType = aroundsType;
    }

    public ArrayList<Cell> initialize(ArrayList<Cell> aliveCell){
        boolean[][] gameBoard = new boolean[w][h];
        ArrayList<Cell> survivingCells = new ArrayList<>(0);
        for (Cell current : aliveCell) {
            gameBoard[current.x+1][current.y+1] = true;
        }
        for (int i=1; i< w-1; i++) {
            for (int j=1; j< h-1; j++) {
                this.doMoore(gameBoard, i, j, survivingCells);
            }
        }
        return survivingCells;
    }

    @Override
    public void doMoore(boolean[][] gameBoard, int i, int j, ArrayList<Cell> survivingCells) {
        int surrounding = 0;
        if (gameBoard[i-1][j-1]) { surrounding++; }
        if (gameBoard[i-1][j])   { surrounding++; }
        if (gameBoard[i-1][j+1]) { surrounding++; }
        if (gameBoard[i][j-1])   { surrounding++; }
        if (gameBoard[i][j+1])   { surrounding++; }
        if (gameBoard[i+1][j-1]) { surrounding++; }
        if (gameBoard[i+1][j])   { surrounding++; }
        if (gameBoard[i+1][j+1]) { surrounding++; }
        if (gameBoard[i][j]) {
            if ((surrounding == 2) || (surrounding == 3)) {
                survivingCells.add(new Cell(i-1,j-1));
            }
        } else {

            if (surrounding == 3) {
                survivingCells.add(new Cell(i-1,j-1));
            }
        }
    }

    @Override
    public void doMargolus(boolean[][] gameBoard, int i, int j, ArrayList<Cell> survivingCells) {

    }

    @Override
    public void doVonNeumann(boolean[][] gameBoard, int i, int j, ArrayList<Cell> survivingCells) {

    }

    @Override
    public void doHexagonal(boolean[][] gameBoard, int i, int j, ArrayList<Cell> survivingCells) {

    }
}
