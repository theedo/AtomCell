package Automata;

import Arounds.AroundsType;
import Arounds.*;

import java.util.ArrayList;
import Grid.Cell.Cell;


public class LiveFreeOrDIe implements AroundsType {

    private int aroundsType;
    private int h;
    private int w;

    public LiveFreeOrDIe(int weight, int hight, int aroundsType){
        this.h = hight;
        this.w = weight;
        this.aroundsType = aroundsType;
    }

    public ArrayList<Cell> initialize(ArrayList<Cell> aliveCell){
        boolean[][] gameBoard = new boolean[w][h];
        ArrayList<Cell> survivingCells = new ArrayList<>(0);
        for (Cell c: aliveCell){
            gameBoard[c.x+1][c.y+1] = true;
        }
        for (int i=1; i<w-1; i++){
            for (int j=1; j<h-1; j++){
                switch (this.aroundsType){
                    case 1:
                        this.doVonNeumann(gameBoard, i, j, survivingCells);
                        break;
                    case 2:
                        this.doMoore(gameBoard, i, j, survivingCells);
                        break;
                    case 3:
                        this.doMargolus(gameBoard, i, j, survivingCells);
                        break;
                }
            }
        }
        return survivingCells;
    }

    @Override
    public void doMoore(boolean[][] gameBoard, int i, int j, ArrayList<Cell> survivingCells) {
        int surrounding = 0;
        if (gameBoard[i-1][j-1]) { surrounding++; }
        if (gameBoard[i-1][j])   { surrounding++; }
        if (gameBoard[i-1][j+1]) { surrounding++; }
        if (gameBoard[i][j-1])   { surrounding++; }
        if (gameBoard[i][j+1])   { surrounding++; }
        if (gameBoard[i+1][j-1]) { surrounding++; }
        if (gameBoard[i+1][j])   { surrounding++; }
        if (gameBoard[i+1][j+1]) { surrounding++; }
        if (gameBoard[i][j]) {
            if (surrounding == 0) {
                survivingCells.add(new Cell(i - 1, j - 1));
            }
        }else {
            if (surrounding == 2){
                survivingCells.add(new Cell(i, j));
            }
        }
    }

    @Override
    public void doMargolus(boolean[][] gameBoard, int i, int j, ArrayList<Cell> survivingCells) {
        int surrounding = 0;
        if(gameBoard[i+1][j-1]){ surrounding++; }
        if(gameBoard[i][j-1]){ surrounding++; }
        if(gameBoard[i+1][j]) { surrounding++; }
        if (gameBoard[i][j]) {
            if (surrounding == 0) {
                survivingCells.add(new Cell(i - 1, j - 1));
            }
        }else {
            if (surrounding == 2){
                survivingCells.add(new Cell(i, j));
            }
        }
    }

    @Override
    public void doVonNeumann(boolean[][] gameBoard, int i, int j, ArrayList<Cell> survivingCells) {
        int surrounding = 0;
        if (gameBoard[i-1][j])   { surrounding++; }
        if (gameBoard[i][j-1])   { surrounding++; }
        if (gameBoard[i][j+1])   { surrounding++; }
        if (gameBoard[i+1][j])   { surrounding++; }
        if (gameBoard[i][j]) {
            if (surrounding == 0){
                survivingCells.add(new Cell(i-1, j-1));
            }
        } else {
            if (surrounding == 2){
                survivingCells.add(new Cell(i, j));
            }
        }
    }

    @Override
    public void doHexagonal(boolean[][] gameBoard, int i, int j, ArrayList<Cell> survivingCells) {
        int surronding = 0;
        if (gameBoard[i][j-1])      { surronding++; }
        if (gameBoard[i+1][j])      { surronding++; }
        if (gameBoard[i+1][j+1])    { surronding++; }
        if (gameBoard[i][j+1])      { surronding++; }
        if (gameBoard[i-1][j])      { surronding++; }
        if (gameBoard[i-1][j-1])    { surronding++; }
        if (gameBoard[i][j]) {
            if (surronding == 0){
                survivingCells.add(new Cell(i-1, j-1));
            }
        } else {
            if (surronding == 2){
                survivingCells.add(new Cell(i, j));
            }
        }
    }
}
