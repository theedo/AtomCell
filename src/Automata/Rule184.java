package Automata;

import Arounds.RuleType;
import Grid.Cell.Cell;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Rule184 implements RuleType {

    private int h;
    private int w;

    public Rule184(int weight, int hight){
        this.h = hight;
        this.w = weight;
    }

    public ArrayList<Cell> initialize(ArrayList<Cell> aliveCell){
        boolean[][] gameBoard = new boolean[w][h];
        ArrayList<Cell> survivingCells = new ArrayList<>(0);
        for (Cell c: aliveCell){
            if(c.x < w-1 && c.y < h-1) {
                gameBoard[c.x + 1][c.y + 1] = true;
            }
        }
        for (int i=1; i<w-1; i++){
            for (int j=1; j<h-1; j++){
                this.rule(gameBoard, i, j, survivingCells);
            }
        }
        return survivingCells;
    }

    @Override
    public void rule(boolean[][] gameBoard, int i, int j, ArrayList<Cell> survivingCells) {
        int surrounding = 0;
        String rule = "";
        List live = Arrays.asList("111", "101", "100", "011");
        if(gameBoard[i-1][j]){
            rule += "1";
        } else {
            rule += "0";
        }
        if(gameBoard[i][j]){
            rule += "1";
        } else {
            rule += "0";
        }

        if(gameBoard[i+1][j]){
            rule += "1";
        } else {
            rule += "0";
        }
        //if (gameBoard[i][j]) {
        if(gameBoard[i][j]) {
            survivingCells.add(new Cell(i - 1, j - 1));
        }
        if(i < w-1 && j < h-1) {
            if (live.contains(rule)) {
                if(j+1 < h-1) {
                    survivingCells.add(new Cell(i - 1, j));
                }
            }
        }
    }
}
