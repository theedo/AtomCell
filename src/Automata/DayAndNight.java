package Automata;

import Arounds.AroundsType;
import Grid.Cell.Cell;

import java.util.ArrayList;

public class DayAndNight implements AroundsType{


    private int aroundsType;
    private int h;
    private int w;

    public DayAndNight(int weight, int hight, int aroundsType){
        this.h = hight;
        this.w = weight;
        this.aroundsType = aroundsType;
    }

    public ArrayList<Cell> initialize(ArrayList<Cell> aliveCell){
        boolean[][] gameBoard = new boolean[w][h];
        ArrayList<Cell> survivingCells = new ArrayList<>(0);
        for (Cell c: aliveCell){
            gameBoard[c.x+1][c.y+1] = true;
        }
        for (int i=1; i<w-1; i++){
            for (int j=1; j<h-1; j++){
                this.doMoore(gameBoard, i, j, survivingCells);
            }
        }
        return survivingCells;
    }

    @Override
    public void doMoore(boolean[][] gameBoard, int i, int j, ArrayList<Cell> survivingCells) {
        int surrounding = 0;
        if (gameBoard[i-1][j-1]) { surrounding++; }
        if (gameBoard[i-1][j])   { surrounding++; }
        if (gameBoard[i-1][j+1]) { surrounding++; }
        if (gameBoard[i][j-1])   { surrounding++; }
        if (gameBoard[i][j+1])   { surrounding++; }
        if (gameBoard[i+1][j-1]) { surrounding++; }
        if (gameBoard[i+1][j])   { surrounding++; }
        if (gameBoard[i+1][j+1]) { surrounding++; }
        if(gameBoard[i][j]) {
            if (surrounding == 3 || surrounding == 4 || surrounding == 6 || surrounding == 7 || surrounding == 8) {
                survivingCells.add(new Cell(i-1, j-1));
            }
        } else {
            if(surrounding == 3 || surrounding == 6 || surrounding == 7 || surrounding == 8){
                survivingCells.add(new Cell(i-1, j-1));
            }
        }
    }


    @Override
    public void doMargolus(boolean[][] gameBoard, int i, int j, ArrayList<Cell> survivingCells) {

    }

    @Override
    public void doVonNeumann(boolean[][] gameBoard, int i, int j, ArrayList<Cell> survivingCells) {

    }

    @Override
    public void doHexagonal(boolean[][] gameBoard, int i, int j, ArrayList<Cell> survivingCells) {

    }
}
